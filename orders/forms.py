from django import forms
from .models import Order

class OrderItemForm(forms.ModelForm):  

    def __init__(self, *args, **kwargs):
        super(OrderItemForm, self).__init__(*args, **kwargs)
        self.fields['type_service'].label = "Tipo de Orden"
        self.fields['start_date'].label = "Fecha de Entrada"
        self.fields['start_date'].widget.attrs = {'class': 'datepicker'}
         
        self.fields['end_date'].label = "Fecha de Salida"
        self.fields['end_date'].widget.attrs = {'class': 'datepicker'}
        self.fields['vehicle'].label = "Vehiculo"
        self.fields['client'].label = "Chofer"
        self.fields['start_oil'].label = "Litros de Gasolina - Inicial"
        self.fields['end_oil'].label = "Litros de Gasolina - Entregado"
        self.fields['is_paid'].label = "Cobranza"
        self.fields['reason'].label = "Motivo"
        self.fields['year'].label = "Año fiscal"
        self.fields['reason'].widget.attrs = {'rows': 2}

    class Meta:
        model = Order
        fields = ['type_service', 'start_date', 'end_date', 'vehicle', 'client', 'km', 'start_oil', 'end_oil', 'reason', 'status', 'is_paid', 'year']

       
