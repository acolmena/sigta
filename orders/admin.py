from django.contrib import admin

from .models import Order, Inventory, Inventory_Order, Inspection_Order, Part_Order, Service_Order

class CustomOrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'vehicle', 'client', 'type_service' ,'start_date', 'km', 'start_oil', 'end_oil', 'reason', 'status', 'is_paid', 'year', )
    search_fields = ('vehicle',)


admin.site.register(Order, CustomOrderAdmin) 
admin.site.register(Inventory)
admin.site.register(Inventory_Order)
admin.site.register(Inspection_Order)
admin.site.register(Part_Order)
admin.site.register(Service_Order)


  