from django.db import models
from django.contrib.postgres.fields import ArrayField
from clients.models import Client
from type_services.models import TypeService
from vehicles.models import Vehicle
from datetime import datetime

def set_year_default():
    return datetime.now().year

class Order(models.Model):

    STATUS_CHOICE = (
        ('Abierta', 'Abierta'),
        ('Cerrada', 'Cerrada'),
        ('En Proceso', 'En Proceso'),
        ('Entregado', 'Entregado'),
        ('Cancelado', 'Cancelado'),
    )

    PAID_CHOICE = (
        ('Pendiente', 'Pendiente'),
        ('Pagado', 'Pagado'),
    )
     
    type_service = models.ForeignKey(TypeService, on_delete=models.PROTECT)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    vehicle = models.ForeignKey(Vehicle, on_delete=models.PROTECT)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    km = models.IntegerField(default='0')
    start_oil = models.IntegerField(default='0')
    end_oil = models.IntegerField(default='0')
    reason = models.TextField(max_length=500)
    status = models.CharField(choices=STATUS_CHOICE, default='Abierta')
    is_paid = models.CharField(choices=PAID_CHOICE, default='Pendiente')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    year = models.IntegerField(default=set_year_default)

    def __str__(self):
        return self.vehicle.plate

    class Meta:
        ordering = ['-created_at']

class Inventory(models.Model):
    class Meta:
        verbose_name = 'Inventory'
        verbose_name_plural = 'inventories'

    inventory = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.inventory

class Inventory_Order(models.Model):
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    inventory = ArrayField(
       models.CharField()
   )



class Inspection_Order(models.Model):
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    inspection = models.TextField()

    def __str__(self):   
        return self.inspection


class Part_Order(models.Model):  
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    part = models.CharField(max_length=200, unique=True)
    quantity = models.IntegerField(default='0')
    amount = models.IntegerField(default='0')

    def __str__(self):   
        return self.part

class Service_Order(models.Model):  
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    service = models.CharField(max_length=200, unique=True)
    amount = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):   
        return self.service

