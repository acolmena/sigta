from django.urls import path
from . import views
from accounts import views as AccountViews

urlpatterns = [
    path('', AccountViews.adminDashboard, name='uadmin'),
    path('informe/', views.informe, name='informe'),
] 