from django.db import models

from brands.models import Brand
from models.models import Model


class Vehicle(models.Model):
    vehicle = models.CharField(max_length=200)
    plate = models.CharField(max_length=15, default='S/P')
    year = models.IntegerField(default='S/A')
    color = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='images')
    brand = models.ForeignKey(Brand, on_delete=models.PROTECT)
    model = models.ForeignKey(Model, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.plate
    
  