from django.contrib import admin

from .models import Vehicle


class CustomVehicleAdmin(admin.ModelAdmin):
    list_display = ('plate', 'vehicle', 'model', 'brand', 'color')
    search_fields = ('place', )
    
admin.site.register(Vehicle, CustomVehicleAdmin)