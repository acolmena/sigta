from django.apps import AppConfig


class InventaryOrdersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'inventary_orders'
