from django.urls import path
from . import views

urlpatterns = [
    path('<int:pk>/', views.brand_detail, name='brand_detail'),
]
