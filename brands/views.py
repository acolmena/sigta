from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from brands.models import Brand


def brand_detail(request, pk):
    brand = get_object_or_404(Brand, pk=pk)
    context = {
        'brand':brand
    }

    return render(request, 'brand_detail.html', context)
       