from django.db import models

class Brand(models.Model):
    brand = models.CharField(max_length=100, unique=True)
    photo = models.ImageField(upload_to='images')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.brand
    
    