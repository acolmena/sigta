from django.contrib import admin

from .models import Client

class CustomClientAdmin(admin.ModelAdmin):
    list_display = ('type_doc' ,'document', 'company_name', 'cell_phone', 'home_phone',)
    search_fields = ('document',)
    
admin.site.register(Client, CustomClientAdmin)
