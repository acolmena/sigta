from django.db import models

from companies.models import Company

class Client(models.Model):

    TYPE_DOC_CHOICE = (
        ('V', 'V'),
        ('J', 'J'),
        ('G', 'G'),
    )

    company = models.ForeignKey(Company, on_delete=models.PROTECT)
    type_doc = models.CharField(choices=TYPE_DOC_CHOICE, default='V')
    document = models.IntegerField(default='0')
    company_name = models.CharField(max_length=150)
    cell_phone = models.CharField(max_length=15)
    home_phone = models.CharField(max_length=15)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.company_name