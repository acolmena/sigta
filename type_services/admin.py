from django.contrib import admin

from .models import TypeService


class CustomTypeServiceAdmin(admin.ModelAdmin):
    list_display = ('type',)
    search_fields = ('type',)
    
admin.site.register(TypeService, CustomTypeServiceAdmin)