from django.db import models

from brands.models import Brand

class Model(models.Model):
    model = models.CharField(max_length=200, unique=True)
    brand = models.ForeignKey(Brand, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.model