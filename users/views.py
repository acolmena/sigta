from django.shortcuts import render, get_object_or_404, redirect
from orders.forms import OrderItemForm
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from reportlab.lib.pagesizes import landscape, letter
from reportlab.platypus import SimpleDocTemplate, Table, Paragraph, Spacer, TableStyle, PageBreak
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from django.db import connection
from datetime import datetime
import locale
from reportlab.lib.units import inch
from vehicles.models import Vehicle
from orders.models import Inventory, Order, Inventory_Order, Inspection_Order, Part_Order, Service_Order


@login_required(login_url='login')
def listarVehiculos(request):
   vehicles = Vehicle.objects.all()
   context = {
        'vehicles': vehicles,
   }
   return render(request, 'users/ListaVehiculos.html', context)

@login_required(login_url='login')
def showOrder(request, pk=None):
  
    order = Order.objects.filter(pk=pk).order_by('-created_at')
    inventary_order = Inventory_Order.objects.filter(order_id=pk)

    inspection_order = Inspection_Order.objects.filter(order_id=pk)
    items_parts_order = Part_Order.objects.filter(order_id=pk)
    items_services_order = Service_Order.objects.filter(order_id=pk)

    context = {
        'order':order,
        'inventary_order':inventary_order,
        'inspection_order':inspection_order,
        'items_parts_order':items_parts_order,
        'items_services_order':items_services_order,
    }

    return render(request, 'users/showOrder.html', context)

@login_required(login_url='login')
def listarOrdenes(request, pk=None):
  
    vehicle = Vehicle.objects.filter(pk=pk)


    query = """
    select oo.id, tst."type" , cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, sum(coalesce(opo.amount,0)) from orders_order oo 
    left join orders_part_order opo on opo.order_id = oo.id
    left join type_services_typeservice tst on oo.type_service_id = tst.id 
    left join clients_client cc on cc.id = oo.client_id 
    where oo.vehicle_id = %s
    group by oo.id, tst."type" , cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year
    order by oo.created_at desc
    ;
    """
    with connection.cursor() as cursor:
        cursor.execute(query, [pk])
        orders = cursor.fetchall()

    context = {
        'vehicle': vehicle,
        'orders':orders
    }
    return render(request, 'users/listarOrdenes.html', context)

@login_required(login_url='login') 
def add_order(request, pk=None):

    if request.method == 'POST':
        form = OrderItemForm(request.POST)

        if form.is_valid():
            form.save()
            
            messages.success(request, 'Orden Agregada con exito!')

            return redirect('listarOrdenes',  pk)
        
        else:
            print(form.errors)
    else:
        form = OrderItemForm()
        
    vehicle = Vehicle.objects.filter(pk=pk)    
    context = {
        'form': form,
        'vehicle': vehicle,
    }
    
    return render(request, 'users/cargarOrdenes.html', context)


@login_required(login_url='login') 
def edit_order(request, pk=None):

    order = get_object_or_404(Order, pk=pk)
    orderVehicle = Order.objects.filter(pk=pk)
    if request.method == 'POST':
        form = OrderItemForm(request.POST, instance=order)

        if form.is_valid():
            form.save()
            
            messages.success(request, 'Orden Actualizada con exito!')

            return redirect('listarVehiculos')
        
        else:
           print(form.errors)
    else:
        form = OrderItemForm(instance=order)
        
    context = {
        'form': form,
        'pk':pk,
        'orderVehicle':orderVehicle
     }

    return render(request, 'users/editarOrder.html', context)

@login_required(login_url='login')
def completed_order(request, pk=None):

    try:
    
        insert_inventory = 'update'
        insert_inspection = 'update'

        order = Order.objects.filter(pk=pk)
        inventory = Inventory.objects.all() 
        inventory_order = Inventory_Order.objects.get(order_id=pk)

        array_queryset = inventory_order.inventory
        for i in range(len(array_queryset)):
            array_queryset[i] = int(array_queryset[i])

    except ObjectDoesNotExist:
        array_queryset = []
        insert_inventory = 'create'

   
    inspection_order = Inspection_Order.objects.filter(order_id=pk)
    
    if not inspection_order:
       inspection_order = {'Inspection_Order':""}
       insert_inspection = 'create'  

    parts_order = Part_Order.objects.filter(order_id=pk)   
    if not parts_order:
       parts_order = {'Part_Order':""}


    context = {
        'order':order,
        'inventory':inventory,
        'inventory_Order':array_queryset,
        'inspection_Order':inspection_order,
        'parts_order':parts_order,
        'insert_inventory':insert_inventory,
        'insert_inspection':insert_inspection,
    }
    
    return render(request, 'users/completedOrder.html', context)

#Admin Completed order
@login_required(login_url='login')
def admin_completed_order(request, pk=None):

    try:
    
        insert_inventory = 'update'
        insert_inspection = 'update'

        order = Order.objects.filter(pk=pk)
        inventory = Inventory.objects.all() 
        inventory_order = Inventory_Order.objects.get(order_id=pk)

        array_queryset = inventory_order.inventory
        for i in range(len(array_queryset)):
            array_queryset[i] = int(array_queryset[i])

    except ObjectDoesNotExist:
        array_queryset = []
        insert_inventory = 'create'

   
    inspection_order = Inspection_Order.objects.filter(order_id=pk)
    
    if not inspection_order:
       inspection_order = {'Inspection_Order':""}
       insert_inspection = 'create'  

    parts_order = Part_Order.objects.filter(order_id=pk)   
    if not parts_order:
       parts_order = {'Part_Order':""}


    context = {
        'order':order,
        'inventory':inventory,
        'inventory_Order':array_queryset,
        'inspection_Order':inspection_order,
        'parts_order':parts_order,
        'insert_inventory':insert_inventory,
        'insert_inspection':insert_inspection,
    }
    
    return render(request, 'users/adminCompletedOrder.html', context)



@login_required(login_url='login')
def create_order(request):

    
    if request.method == 'POST':
        
        order=request.POST["order"]

        insert_inventory=request.POST["insert_inventory"]
        inventory=request.POST.getlist('inventory[]')

        insert_inspection=request.POST["insert_inspection"]
        
        inspection=request.POST["inspection"]

        parts=request.POST.getlist('descripcion[]')
        quantity=request.POST.getlist('cantidad[]')
        monto=request.POST.getlist('monto[]')
        
        
        if (insert_inventory == 'create'):
            Inventory_Order.objects.create(inventory=inventory, order_id=order)
        else:
            Inventory_Order.objects.filter(order_id=order).update(inventory=inventory)

        if (insert_inspection == 'create'):
            if inspection:
                Inspection_Order.objects.create(inspection=inspection, order_id=order)
        else:    
            Inspection_Order.objects.filter(order_id=order).update(inspection=inspection)

        Part_Order.objects.filter(order_id=order).delete()     
        for i in range(len(parts)):
            part=parts[i]
            qty=int(quantity[i],0)
            amount=int(monto[i],0)
            Part_Order.objects.create(part=part, quantity=qty, amount=amount, order_id=order, ) 
    else:
        return render(request, 'accounts/login.html')
    
    return redirect('listarVehiculos')

#dashboard Users
@login_required(login_url='login')
def order_type_service(request, pk=None, date=None):

    if date is not None and date != '':
        date_list = date.split('-')
        year = date_list[0]
        month = date_list[1]

    query = """
    select oo.id, tst."type" ,cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, vv.plate, sum(coalesce(opo.amount,0)) from orders_order oo 
    left join orders_part_order opo on opo.order_id = oo.id
    left join type_services_typeservice tst on oo.type_service_id = tst.id 
    left join clients_client cc on cc.id = oo.client_id 
    left join vehicles_vehicle vv on vv.id = oo.vehicle_id 
    where oo.type_service_id = %s and
    EXTRACT(YEAR FROM oo.start_date) = %s
    and EXTRACT(MONTH FROM oo.start_date) = %s
    group by oo.id, tst."type" , cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, vv.plate
    order by oo.created_at desc;
    """
    with connection.cursor() as cursor:
        cursor.execute(query, [pk, year, month])
        orders = cursor.fetchall()

    context = {
        'orders':orders
    }

    return render(request, 'users/typeServicioOrder.html', context)

#dashboard users
@login_required(login_url='login')
def order_status(request, status=None, date=None):

    if date is not None and date != '':
        date_list = date.split('-')
        year = date_list[0]
        month = date_list[1]

    query = """
    select oo.id, tst."type", cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, vv.plate, sum(coalesce(opo.amount,0)) from orders_order oo 
    left join orders_part_order opo on opo.order_id = oo.id
    left join type_services_typeservice tst on oo.type_service_id = tst.id 
    left join clients_client cc on cc.id = oo.client_id 
    left join vehicles_vehicle vv on vv.id = oo.vehicle_id 
    where oo.is_paid = %s and 
    EXTRACT(YEAR FROM oo.start_date) = %s
    and EXTRACT(MONTH FROM oo.start_date) = %s
    group by oo.id, tst."type" , cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, vv.plate
    order by oo.created_at desc;
    """
    with connection.cursor() as cursor:
        cursor.execute(query, [status, year, month])
        orders = cursor.fetchall()

    context = {
        'orders':orders
    }
    return render(request, 'users/statusOrder.html', context)



#dashboard Admin
@login_required(login_url='login')
def admin_order_type_service(request, pk=None, date=None):

    if date is not None and date != '':
        date_list = date.split('-')
        year = date_list[0]
        month = date_list[1]

    query = """
    select oo.id, tst."type" , cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, vv.plate, sum(coalesce(opo.amount,0)) from orders_order oo 
    left join orders_part_order opo on opo.order_id = oo.id
    left join type_services_typeservice tst on oo.type_service_id = tst.id 
    left join clients_client cc on cc.id = oo.client_id 
    left join vehicles_vehicle vv on vv.id = oo.vehicle_id 
    where oo.type_service_id = %s
    and EXTRACT(YEAR FROM oo.start_date) = %s
    and EXTRACT(MONTH FROM oo.start_date) = %s
    group by oo.id, tst."type", cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, vv.plate
    order by oo.created_at desc;
    """
    with connection.cursor() as cursor:
        cursor.execute(query, [pk, year, month])
        orders = cursor.fetchall()

    context = {
        'orders':orders
    }

    return render(request, 'users/AdminTypeServicioOrder.html', context)

#dashboard Admin
@login_required(login_url='login')
def admin_order_status(request, status=None, date=None):

    if date is not None and date != '':
        date_list = date.split('-')
        year = date_list[0]
        month = date_list[1]

    query = """
    select oo.id, tst."type", cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, vv.plate, sum(coalesce(opo.amount,0)) from orders_order oo 
    left join orders_part_order opo on opo.order_id = oo.id
    left join type_services_typeservice tst on oo.type_service_id = tst.id 
    left join clients_client cc on cc.id = oo.client_id 
    left join vehicles_vehicle vv on vv.id = oo.vehicle_id 
    where oo.is_paid = %s
    and EXTRACT(YEAR FROM oo.start_date) = %s
    and EXTRACT(MONTH FROM oo.start_date) = %s
    group by oo.id, tst."type" , cc.company_name, oo.reason , oo.start_date , oo.km , oo.start_oil , oo.end_oil , oo.status , oo.is_paid, oo.year, vv.plate
    order by oo.created_at desc
    ;
    """
    with connection.cursor() as cursor:
        cursor.execute(query, [status, year, month])
        orders = cursor.fetchall()

    context = {
        'orders':orders
    }

    return render(request, 'users/AdminStatusOrder.html', context)

@login_required(login_url='login')
def generate_order_pdf(request, pk=None):
    
    locale.setlocale(locale.LC_ALL, 'es_ES.UTF-8')
    styles = getSampleStyleSheet()

    order = Order.objects.filter(id=pk)
    parts = Part_Order.objects.filter(order_id=pk)
    inspection = Inspection_Order.objects.filter(order_id=pk)

    items_data_inventory = []

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = f'attachment; filename="order_{pk}.pdf"'

    doc = SimpleDocTemplate(response, pagesize=letter)

    #order
    styles = getSampleStyleSheet()

    style_bold = styles['Heading1'].clone('Title')
    style_bold.fontSize = 24
    style_bold.textColor = colors.black
    style_bold.alignment = 1

    title = Paragraph("Detalle de la orden", style=style_bold)

    content = []

    content.append(title)

    content.append(Spacer(1, 12))
    content.append(Paragraph(f'Orden #: {pk}', styles['Normal']))

    for item in order: 
        content.append(Paragraph(f'Servicio: {item.type_service} - Vehiculo  {item.vehicle}', styles['Normal']))
        content.append(Paragraph(f'Chofer: {item.client}', styles['Normal']))
        content.append(Paragraph(f'Fecha de entrada: {item.start_date}', styles['Normal']))
        content.append(Paragraph(f'Fecha de salida: {item.end_date}', styles['Normal']))
        content.append(Paragraph(f'Pagado: {item.is_paid}', styles['Normal'].clone('Bold')))
        

    content.append(Spacer(1, 12))

    # content.append(draw_line)

    #parts
    item_data = [["Descripcion", "Cantidad", "Monto"]]

    total = 0
    for item in parts:
        total += item.amount
        item_data.append([
            item.part,
            item.quantity,
            locale.format_string('%.2f', item.amount, grouping=True),
        ])

    item_data.append(['', 'Total General:', locale.format_string('%.2f', total, grouping=True)])   

    items_table = Table(item_data, colWidths=[350, 90, 90])

    style = TableStyle([('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                        ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
                        ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
                        ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
                        ('BACKGROUND', (0, 1), (-1, -1), colors.white),
                        ('GRID', (0, 0), (-1, -1), 1, colors.black)])

    items_table.setStyle(style)
    content.append(items_table)
    #content.append(Table(item_data, colWidths=[450, 25]))

    content.append(Spacer(1, 12))
    #Inspection
    item_data = [["Inspeccion"]]

    for item in inspection:
        content.append(Paragraph(f'Estado general del vehiculo: {item.inspection}', styles['Normal']))

    content.append(Spacer(1, 12))

    inventory_order = Inventory_Order.objects.get(order_id=pk)
    inventories = Inventory.objects.all() 

    array_queryset = inventory_order.inventory
    for i in range(len(array_queryset)):
        array_queryset[i] = int(array_queryset[i])
    
    for i in inventories:
        if i.id in array_queryset:
            items_data_inventory.append(i.inventory)    
    
    #inventories
    invetory_item_data = [["Inventario del vehiculo"]]

    for item in items_data_inventory:
        invetory_item_data.append([
             item
        ])

    inventory_items_table = Table(invetory_item_data, colWidths=[530])


    style = TableStyle([('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                        ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
                        ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
                        ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
                        ('BACKGROUND', (0, 1), (-1, -1), colors.white),
                        ('GRID', (0, 0), (-1, -1), 1, colors.black)])

    inventory_items_table.setStyle(style)

    content.append(inventory_items_table)                

    doc.build(content)
    return response

def monthsYear(month):
    months = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    return months[month]

def add_page_number_and_date(canvas, doc):
    # Agregar número de página
    page_num = canvas.getPageNumber()
    text = f"Página {page_num}"
    canvas.drawRightString(letter[0] - inch, 0.75 * inch, text)

    fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    text = f"Impreso el: {fecha_actual}"
    canvas.drawString(inch, 0.75 * inch, text)


def order_details_pdf(request, date=None):

    locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
    
    if date is not None and date != '':
        date_list = date.split('-')
        year = date_list[0]
        month = date_list[1]

        monthReport = "Mes de:" + monthsYear(int(month))
       

    query = """
    	select oo.id, vv.plate , vv.vehicle, bb.brand , mm.model , oo.reason , oo.is_paid from orders_order oo 
        inner join vehicles_vehicle vv 
        on oo.vehicle_id = vv.id 
        inner join brands_brand bb 
        on vv.brand_id = bb.id 
        inner join models_model mm 
        on mm.id = vv.model_id 
        where 
        EXTRACT(YEAR FROM oo.start_date) = %s
        and EXTRACT(MONTH FROM oo.start_date) = %s
        order by oo.id , vv.plate ,oo.is_paid 
    """
    with connection.cursor() as cursor:
        cursor.execute(query, [year, month])
        orders = cursor.fetchall()

    data = []
    total_general = 0
    for x in orders:
        
        parts = Part_Order.objects.filter(order_id=x[0])
        parts_order = []
        total = 0
        if parts.exists():
            for i in parts:
                part = {
                        'order_id': i.order_id,
                        'part': i.part,
                        'qty': i.quantity,
                        'amount': i.amount
                    }
                total += i.amount
                parts_order.append(part)
            total_general += total
            data.append({
                'id': x[0],
                'plate': x[1],
                'vehicle': x[2],
                'brand': x[3],
                'model': x[4],
                'reason': x[5],
                'is_paid': x[6],
                'parts':parts_order,
                'total':total
            })

    data.append({
               'total_general':total_general
            })


    tabla_datos = []

    tabla_datos.append(['ID', 'Placa', 'Vehículo', 'Marca', 'Modelo', 'Razón', 'Estado de pago', 'Total'])

    for entry in data:
        totalParts = 0
        if 'id' in entry:
            tabla_datos.append([entry['id'], entry['plate'], entry['vehicle'], entry['brand'], entry['model'], entry['reason'], entry['is_paid'], ''])
            for part in entry['parts']:
                tabla_datos.append(['', '', '', '', '', part['part'], part['qty'], part['amount']])
                totalParts += part['amount']
            
            tabla_datos.append(['', '', '', '', '', 'SubTotal', '', totalParts])

        elif 'total_general' in entry:
            tabla_datos.append(['' for _ in range(8)])
            tabla_datos.append(['', '', '', '', '', 'Total General', '', entry['total_general']])

    # Crear el PDF
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="datos.pdf"'

    doc = SimpleDocTemplate(response, pagesize=landscape(letter))

    # Estilos para el título y subtítulo
    styles = getSampleStyleSheet()
    style_title = styles['Title']
    style_subtitle = styles['Heading2']

    fecha_actual = datetime.now().strftime("%d-%B-%Y %H:%M:%S")
    fecha_actual = Paragraph(f"Fecha de impresión: {fecha_actual}", styles['Normal'])
    # Agregar título y subtítulo al PDF
    title = Paragraph("Detalle de las órdenes", style_title)
    subtitle = Paragraph(monthReport, style_subtitle)

       # Agregar fecha de impresión al PDF

    table = Table(tabla_datos)

    # Establecer estilo de la tabla
    style = TableStyle([('GRID', (0, 0), (-1, -1), 1, (0, 0, 0)),
                        ('BACKGROUND', (0, 0), (-1, 0), '#CCCCCC'),
                        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
                        ('BOTTOMPADDING', (0, 0), (-1, 0), 12)])

    table.setStyle(style)
    doc.build([fecha_actual,title, subtitle, table])

    return response


           


       