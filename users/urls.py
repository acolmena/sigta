from django.urls import path
from . import views
from accounts import views as AccountViews

urlpatterns = [
    path('', AccountViews.adminDashboard, name='uadmin'),
    path('listarVehiculos/', views.listarVehiculos, name='listarVehiculos'),
    path('listarOrdenes/<int:pk>/', views.listarOrdenes, name='listarOrdenes'),
    path('order-new/order/add/<int:pk>/', views.add_order, name='add_order'),
    path('order-edit/order/edit/<int:pk>/', views.edit_order, name='edit_order'),
    path('order-pdf/order/pdf/<int:pk>/', views.generate_order_pdf, name='pdf_order'),
    path('order-show/order/show/<int:pk>/', views.showOrder, name='show_order'),
    path('order-completed/order/completed/<int:pk>/', views.completed_order, name='completed_order'),
    path('order-create/order/create/', views.create_order, name='create_order'),

    #dashboard Users
    path('order-type-service/order/type-service/<int:pk>/<str:date>/', views.order_type_service, name='order_type_service'),
    path('order-type-service/order/status/<str:status>/<str:date>/', views.order_status, name='order_status'),
    path('order-details/order/details/<str:date>/', views.order_details_pdf, name='order_details_pdf'),
    #dashboard Admin
    path('admin-order-type-service/order/type-service/<int:pk>/<str:date>/', views.admin_order_type_service, name='admin_order_type_service'),
    path('admin-order-type-service/order/status/<str:status>/<str:date>/', views.admin_order_status, name='admin_order_status'),
    path('admin-order-completed/order/completed/<int:pk>/', views.admin_completed_order, name='admin_completed_order'),
    

] 
