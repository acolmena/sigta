from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.contrib import messages, auth

from accounts.utils import detectUser
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import PermissionDenied
from orders.models import Order
from type_services.models import TypeService
from django.db import connection
from datetime import datetime

def check_role_admin(user):
    if user.role == 1:
        return True
    else:
        raise PermissionDenied
    
def check_role_user(user):
    if user.role == 2:
        return True
    else:
        raise PermissionDenied

def loginUser(request):
    if request.user.is_authenticated:
        messages.warning(request, 'Ya se ha autentificado!')
        return redirect('myAccount')
    elif request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        user = auth.authenticate(email=email, password=password)

        if user is not None:
            auth.login(request, user)
            messages.success(request, 'Usuario Autorizado....')
            return redirect('myAccount')
        else:
            messages.error(request, 'Error en las credenciales')
            return redirect('login')
    return render(request, 'accounts/login.html')

def logoutUser(request):
    auth.logout(request)
    messages.info(request, 'Hasta pronto...')
    return redirect('login')

@login_required(login_url='login')
def myAccount(request):
    user = request.user
    redirectUrl = detectUser(user)
    return redirect(redirectUrl)

@login_required(login_url='login')
def userDashboard(request):

    date = request.POST.get('date', False)

    if date:
        date_list = date.split('-')
        year = date_list[0]
        month = date_list[1] 
    else:
        fecha_actual = datetime.now()
        year = fecha_actual.year
        month = fecha_actual.month

    date_current = str(year) + "-" + str(month)

    query = """
    select tst.id, tst.type , count(oo.id) as total from orders_order oo 
    inner join type_services_typeservice tst on
    oo.type_service_id = tst.id 
    where 
    EXTRACT(YEAR FROM oo.start_date) = %s
    and EXTRACT(MONTH FROM oo.start_date) = %s
    group by tst.id, tst."type"
    order by tst.id;
    """
    
    with connection.cursor() as cursor:
        cursor.execute(query, [year, month])
        rows_order = cursor.fetchall()

    query = """
    select detalle.is_paid, sum(detalle.total), count(detalle.id) from (select oo.id , oo.is_paid , sum(opo.amount) as total from orders_order oo 
    inner join orders_part_order opo on oo.id = opo.order_id 
    where 
    EXTRACT(YEAR FROM oo.start_date) = %s
    and EXTRACT(MONTH FROM oo.start_date) = %s
    group by oo.id) as detalle
    group by detalle.is_paid;
    """
    
    with connection.cursor() as cursor:
        cursor.execute(query, [year, month])
        rows_is_paid = cursor.fetchall()    
    
    context = {
        'results':rows_order,
        'is_paid':rows_is_paid,
        'fecha_actual': date_current
    }

    return render(request, 'dashboard/userDashboard.html', context)

@login_required(login_url='login')
@user_passes_test(check_role_admin)
def adminDashboard(request):

    date = request.POST.get('date', False)

    if date:
        date_list = date.split('-')
        year = date_list[0]
        month = date_list[1] 
    else:
        fecha_actual = datetime.now()
        year = fecha_actual.year
        month = fecha_actual.month

    date_current = str(year) + "-" + str(month)

    query = """
    select tst.id, tst.type , count(oo.id) as total from orders_order oo 
    inner join type_services_typeservice tst on
    oo.type_service_id = tst.id 
    where EXTRACT(YEAR FROM oo.start_date) = %s
    and EXTRACT(MONTH FROM oo.start_date) = %s
    group by tst.id, tst."type"
    order by tst.id;
    """
    
    with connection.cursor() as cursor:
        cursor.execute(query, [year, month])
        rows_order = cursor.fetchall()

    query = """
    select detalle.is_paid, sum(detalle.total), count(detalle.id) from (select oo.id , oo.is_paid , sum(opo.amount) as total from orders_order oo 
    inner join orders_part_order opo on oo.id = opo.order_id 
    where 
    EXTRACT(YEAR FROM oo.start_date) = %s
    and EXTRACT(MONTH FROM oo.start_date) = %s
    group by oo.id) as detalle
    group by detalle.is_paid;
    """
    
    with connection.cursor() as cursor:
        cursor.execute(query, [year, month])
        rows_is_paid = cursor.fetchall()    
    
    context = {
        'results':rows_order,
        'is_paid':rows_is_paid,
        'fecha_actual': date_current,
    }
    return render(request, 'dashboard/adminDashboard.html', context)