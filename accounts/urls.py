from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.myAccount, name='myAccount'),
    path('login/', views.loginUser, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path('myAccount/', views.myAccount, name='myAccount'),
    path('userDashboard/', views.userDashboard, name='userDashboard'),
    path('adminDashboard/', views.adminDashboard, name='adminDashboard'),
    path('admin/', include('admin.urls')),
] 