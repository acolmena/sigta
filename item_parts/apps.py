from django.apps import AppConfig


class ItemPartsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'item_parts'
