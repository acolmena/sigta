from django.contrib import admin

from .models import Company


class CustomCompanyAdmin(admin.ModelAdmin):
    list_display = ('company',)
    search_fields = ('company',)
    
admin.site.register(Company, CustomCompanyAdmin)